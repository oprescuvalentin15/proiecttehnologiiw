// public/core.js
var Organization = angular.module('Organization', []);

function mainController($scope, $http) {
    $scope.formData = {};

    // when landing on the page, get all organiz and show them
    $http.get('/api/organiz')
        .success(function(data) {
            $scope.organiz = data;
            console.log(data);
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });

    // when submitting the add form, send the text to the node API
    $scope.createOrganiz = function() {
        $http.post('/api/organiz', $scope.formData)
            .success(function(data) {
                $scope.formData = {}; // clear the form so our user is ready to enter another
                $scope.organiz = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

    // delete a organiz after checking it
    $scope.deleteOrganiz = function(id) {
        $http.delete('/api/organiz/' + id)
            .success(function(data) {
                $scope.organiz = data;
                console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
    };

}

