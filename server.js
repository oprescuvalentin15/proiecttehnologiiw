// server.js

    // set up ========================
    var express  = require('express');
    var app      = express();                               
    var mongoose = require('mongoose');                     
    var morgan = require('morgan');             
    var bodyParser = require('body-parser');    
    var methodOverride = require('method-override'); 

    // configuration =================

    mongoose.connect('mongodb://node:nodeuser@mongo.onmodulus.net:27017/uwO3mypu');     

    app.use(express.static(__dirname + '/public'));                 
    app.use(morgan('dev'));                                         
    app.use(bodyParser.urlencoded({'extended':'true'}));            
    app.use(bodyParser.json());                                     
    app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 
    app.use(methodOverride());
    
    var Org = mongoose.model('Org', {
        text : String
    });

app.get('/api/organiz', function(req, res) {

        // use mongoose to get all organiz in the database
        Org.find(function(err, organiz) {

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err)

            res.json(organiz); // return all orgniz in JSON format
        });
    });

    // create organiz and send back all organiz after creation
    app.post('/api/organiz', function(req, res) {

        // create a organiz, information comes from AJAX request from Angular
        Org.create({
            text : req.body.text,
            done : false
        }, function(err, organiz) {
            if (err)
                res.send(err);

            // get and return all the organizations after you create another
            Org.find(function(err, organiz) {
                if (err)
                    res.send(err)
                res.json(organiz);
            });
        });

    });

    // delete a organization
    app.delete('/api/organiz/:organiz_id', function(req, res) {
        Org.remove({
            _id : req.params.organiz_id
        }, function(err, organiz) {
            if (err)
                res.send(err);

            // get and return all the organiz after you create another
            Org.find(function(err, organiz) {
                if (err)
                    res.send(err)
                res.json(organiz);
            });
        });
    });
    
    // application -------------------------------------------------------------
    app.get('*', function(req, res) {
        res.sendfile('./public/index.html'); 
    });

    // listen (start app with node server.js) ======================================
    app.listen(8080);
    console.log("App listening on port 8080");


